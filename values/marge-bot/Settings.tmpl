debug: false
listen_address: "::"
port: 8080

.is_push: &is_push
  # the hook is of kind "push"
  equals:
    key: "object_kind"
    value: "push"

.reload_command: &reload_command
  - "bash"
  - "-c"
  - "cd /shared && curl -LO -f --retry 4 --retry-delay 60 {{ SCRIPT_URL }} && bash /shared/prep.sh && pkill -9 run_marge.sh"

.reload_env: &reload_env
  - "SCRIPT_URL"
  - "NAME"
  - "SETTINGS"
  - "PROJECT_REPO"
  - "SECRETS_FILE"

# define a global workload for reloading the hook configuration
.autoreload_config_on_push: &autoreload_config_on_push
  name: update when push
  description: |
    update all config when there is a push, and reload
    hookiedookie
  rule:
    *is_push
  run:
    *reload_command
  env:
    *reload_env

.autoreload_config_on_fdo_bots_push: &autoreload_config_on_fdo_bots_push
  name: update when push on fdo_bots
  description: |
    update all config when there is a push touching selected files,
    and reload hookiedookie
  run:
    *reload_command
  env:
    *reload_env
  rule:
    and:
      - *is_push
      - any:
          key: commits
          op:
            or:
              - any:
                  key: modified
                  op:
                    substring:
                      needle: values/hookiedookie
              - any:
                  key: added
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}
              - any:
                  key: modified
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}
              - any:
                  key: removed
                  op:
                    substring:
                      needle: values/{{ PROJECT_REPO }}

webhooks:
  - path: "freedesktop/fdo-bots"
    token: "{{ marge_bot.fdo_bots.token}}"

    workloads:
      - *autoreload_config_on_fdo_bots_push

  - path: "freedesktop/marge-bot"
    token: "{{ marge_bot.marge_bot.token}}"

    workloads:
      - *autoreload_config_on_push
